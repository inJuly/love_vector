# Vector2 

A simple 2D vector library in Lua.

# Usage

```lua
Vec2 = require ('vector2')

-- new Vector can by created simply by calling Vec2
local position = Vec2(10, 5)
-- ... or by calling Vec2:new
local velocity = Vec2:new(1, 0)

-- adding two vectors:
position = position + velocity
-- dot product

local v = Vec2(0, 1) * Vec2(1, 0)

-- scaling
local scaledVector = 2 * Vec2(2, 0)
print(scaledVector) -- {x = 4, y = 3}

-- get the normalized vector:

local normed = Vec2(100, 5):normalized()

-- normalize in place:

local myvec = Vec2(10, 10)
myvec:normalize()

-- get angle:

print(myvec:angle())

--  get rotated vector (in radians)
local my_other_vec = Vec2(1, 2)
local new_vec = my_other_vec:rotated(2 * math.pi)

-- rotate in place:
new_vec:rotate(math.pi)

--  get magnitude:

print('magnitude is' .. my_vec:mag())

--  change magnitude in place
my_vec:setMag(3)
--  get new vector with magnitude
my_vec = my_vec:withMag(12)

--  copy a vector instance:
my_vec = my_other_vec:copy()

-- unit vector pointing in random direction:

local my_dir = Vec2.randomUnit()

--  direction vectors:
--  note that screen coordinates start at top-left corner 
--  y increases to the bottom and x to the left.
print(Vec2.RIGHT(), Vec2.LEFT(), Vec2.UP(), Vec2.DOWN())
-- {x = 1, y = 0}    {x = -1, y = 0}    {x = 0, y = -1}    {x = 0, y = 1}

```

# LICENSE

The library is available under the MIT License. You are free to make changes to this implementation to suit your purposes
as long as the license text is included.